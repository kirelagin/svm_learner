import logging
log = logging.getLogger(__name__)
import numpy as np


def evaluate(classify, xs, ys):
    assert len(xs) == len(ys)

    predictions = np.apply_along_axis(classify, 1, xs)

    result = predictions == ys
    correct = np.sum(result)
    log.info('Correct: {}'.format(correct))
    total = len(result)
    log.info('Total:   {}'.format(total))
    percent = correct / total * 100
    log.info('Accuracy: {:.2f}%'.format(percent))

    n = ys.max() + 1
    matr = np.bincount(ys * n + predictions)
    matr.resize((n,n))
    log.info('Predictions: \n{}'.format(matr))

    return percent
