import numpy as np
import os.path


def read(path, setid):
    xfile = os.path.join(path, setid[0], 'X_' + setid[0] + setid[1] + '.txt')
    yfile = os.path.join(path, setid[0], 'y_' + setid[0] + setid[1] + '.txt')
    x = np.loadtxt(xfile)
    y = np.loadtxt(yfile, dtype=np.int8)
    return (x, y - 1)
