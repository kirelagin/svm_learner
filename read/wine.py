import numpy as np
import os.path


def read(path, setid):
    fname = os.path.join(path, setid + '.csv')
    with open(fname, 'rt') as f:
        lines = [l.strip().split(';') for l in f]
    x = np.array([l[:-1] for l in lines], dtype=float)
    y = np.array([l[-1] for l in lines], dtype=int)
    return (x, y)
