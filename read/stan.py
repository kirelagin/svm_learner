import numpy as np
import os.path


def read(path, setname):
    with open(os.path.join(path, setname), 'rt') as f:
        f.readline()
        r, c = map(int, f.readline().split())
        f.readline()
        matrix = np.zeros((r, c))
        category = np.empty(r, dtype=int)

        for m in range(r):
            line = f.readline()
            nums = np.array(list(map(int, line.split())))
            category[m] = 1 if nums[0] == 1 else 0
            matrix[(m, np.cumsum(nums[1:-1:2])-1)] = nums[2:-1:2]

        return (matrix, category)
