import numpy as np
import os.path


def read(path, setid):
    fname = os.path.join(path, setid + '.data')
    with open(fname, 'rt') as f:
        lines = [l.strip().split() for l in f]
    x = np.array([l[:-10] for l in lines], dtype=float)
    yc = np.array([l[-10:] for l in lines], dtype=int)
    y = np.array([np.where(yn==1)[0][0] for yn in yc])
    return (x, y)
