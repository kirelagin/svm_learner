#!/usr/bin/env python3

from datetime import datetime
import logging
log = logging.getLogger(__name__)
import numpy as np
import os.path
import pickle
import mylog

from evaluate import evaluate
from classify import Binary, Ovo, ssmo, smo
import read.stan as stan
import read.har as har
import read.wine as wine
import read.semeion as semeion
from util.kernel import Gaussian, Linear


def load_data(filename):
    dat = np.load(os.path.join('data', filename) + '.npz')
    res = (dat['x'], dat['y'])
    dat.close()
    return res

def read_data(reader, setid):
    path = os.path.join('data', reader.__name__.split('.')[-1])
    return reader.read(path, setid)

def load_classifier(fname):
    with open(os.path.join('runs', fname), 'rb') as f:
        return pickle.load(f)


if __name__ == '__main__':
    mylog.setup(logging.INFO)

    log.info('Reading training data')
    trainX, trainY = read_data(har, ('train', '1000'))
    log.info('Training')
    svm = Ovo(trainX, trainY, smo, Gaussian, (5.953125,))
    log.info('Ready')
    with open(os.path.join('runs', datetime.now().isoformat().replace(':', '-')), 'wb') as f:
        pickle.dump(svm, f)

    log.info('Reading testing data')
    testX, testY = read_data(har, ('test', '1000'))
    log.info('Evaluating')
    evaluate(svm, testX, testY)
