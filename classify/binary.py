class Binary:
    def __init__(self, classifier):
        self._classify = classifier

    def __call__(self, x):
        return 1 if self._classify(x) > 0 else 0
