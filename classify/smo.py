import logging
log = logging.getLogger(__name__)
import numpy as np
import random

from classify.svm import Svm
from util.seq import rndrange, rndshift


def ssmo(x, y, kern, c=0.1, eps=0.001):
    assert len(x) == len(y)
    assert np.logical_or(y == 1, y == 0).all()
    y[y == 0] = -1
    m = len(x)
    a = np.zeros(m)
    b = np.float(0)
    e = np.empty(m)

    def error(k):
        return Svm(a, x, y, b, kern)(x[k]) - y[k]

    def optimize(i, j):
        if y[i] != y[j]:
            l, h = max(0, a[j] - a[i]), min(c, c + a[j] - a[i])
        else:
            l, h = max(0, a[i] + a[j] - c), min(c, a[i] + a[j])

        if l == h:
            log.debug('L == H')
            return False

        eta = 2 * kern(x[i], x[j]) - kern(x[i], x[i]) - kern(x[j], x[j])
        if eta >= 0:
            log.warn('eta >= 0')
            return False

        ajold = a[j]
        a[j] -= y[j] * (e[i] - e[j]) / eta
        if a[j] > h:
            a[j] = h
        elif a[j] < l:
            a[j] = l
        if abs(a[j] - ajold) < 0.00001:
            log.debug('new alpha almost equals old alpha')
            return False

        aiold = a[i]
        a[i] += y[i]*y[j] * (ajold - a[j])

        nonlocal b
        b1 = b - e[i] - y[i] * (a[i] - aiold) * kern(x[i], x[i]) - y[j] * (a[j] - ajold) * kern(x[i], x[j])
        b2 = b - e[j] - y[i] * (a[i] - aiold) * kern(x[i], x[j]) - y[j] * (a[j] - ajold) * kern(x[j], x[j])
        if 0 < a[i] < c:
            b = b1
        elif 0 < a[j] < c:
            b = b2
        else:
            b = (b1 + b2) / 2

        log.debug('Optimized')
        return True

    useless_passes = 0
    while useless_passes < 4:
        changed = False
        for i in rndrange(m):
            e[i] = error(i)
            if (y[i] * e[i] < -eps and a[i] < c) or (y[i] * e[i] > eps and a[i] > 0):
                j = random.randint(0, m-2)
                if j >= i: j += 1
                e[j] = error(j)
                changed |= optimize(i, j)
        if not changed:
            useless_passes += 1
            log.debug('Useless pass #{}'.format(useless_passes))
        else:
            useless_passes = 0

    return Svm(a, x, y, b, kern)


def smo(x, y, kern, c=0.1, eps=0.001):
    assert len(x) == len(y)
    assert np.logical_or(y == 1, y == 0).all()
    y[y == 0] = -1
    m = len(x)
    a = np.zeros(m)
    b = np.float(0)
    e = np.empty(m)
    k = np.array([kern(x[i], x[j]) for i in range(m) for j in range(m)]).reshape((m,m))

    # local variable to speed things up
    notbound = None

    def error(k):
        return Svm(a, x, y, b, kern)(x[k]) - y[k]

    def optimize(i, j):
        if i == j: return False

        s = y[i]*y[j]

        if y[i] != y[j]:
            l, h = max(0, a[j] - a[i]), min(c, c + a[j] - a[i])
        else:
            l, h = max(0, a[i] + a[j] - c), min(c, a[i] + a[j])

        if l == h:
            log.debug('L == H')
            return False

        if a[i] < eps or a[i] > c - eps:
            e[i] = error(i)

        if a[j] < eps or a[j] > c - eps:
            e[j] = error(j)

        eta = 2 * k[i,j] - k[i,i] - k[j,j]
        ajold = a[j]
        if eta < 0:
            a[j] -= y[j] * (e[i] - e[j]) / eta
            if a[j] > h:
                a[j] = h
            elif a[j] < l:
                a[j] = l
        else:
            log.warn('Eta >= 0. Using dark magic now')
            gamma = a[i] + s * a[j]
            vi = np.sum([y[l] * a[l] * k[i, l] for l in range(m) if l != i and l != j])
            vj = np.sum([y[l] * a[l] * k[j, l] for l in range(m) if l != i and l != j])
            def W(a2):
                t = gamma - s * a2
                return gamma - s * a2 + a2 - 1/2 * k[i,i] * t**2 - 1/2 * k[j,j] * a2**2 - s * k[i,j] * t * a2 - y[i] * t * vi - y[j] * a[j] * vj
            lobj = W(l)
            hobj = W(h)
            if lobj > hobj + eps:
                a[j] = l
            elif lobj < hobj - eps:
                a[j] = h

        if a[j] < 1e-8: a[j] = 0
        if a[j] > c - 1e-8: a[j] = c


        if abs(a[j] - ajold) < 0.00001:
            log.debug('new alpha almost equals old alpha')
            return False

        aiold = a[i]
        a[i] += s * (ajold - a[j])

        nonlocal b
        b1 = b - e[i] - y[i] * (a[i] - aiold) * k[i,i] - y[j] * (a[j] - ajold) * k[i,j]
        b2 = b - e[j] - y[i] * (a[i] - aiold) * k[i,j] - y[j] * (a[j] - ajold) * k[j,j]
        if 0 < a[i] < c:
            b = b1
        elif 0 < a[j] < c:
            b = b2
        else:
            b = (b1 + b2) / 2

        inds = np.where(notbound)[0]
        e[inds] = [error(k) for k in inds]

        log.debug('OPTIMISED')
        return True

    changed = True
    examineAll = False
    while changed or examineAll:
        changed = False
        notbound = np.logical_and(a > eps, a < c - eps)
        loopover = range(m) if examineAll else np.where(notbound)[0]
        for j in rndshift(loopover):
            if examineAll and (a[j] < eps or a[j] > c - eps):
                e[j] = error(j)
            t = y[j] * e[j]
            if (t < -eps and a[j] < c) or (t > eps and a[j] > 0):
                # second choice heuristic
                f = np.max if e[j] < 0 else np.min
                inds = np.where(np.logical_and(e == f(e[notbound]), notbound))[0] if notbound.any() else []
                if len(inds) > 0:
                    if optimize(inds[0], j):
                        changed = True
                        continue
                # fallback second choice
                log.debug('Fallback second choice activated')
                for i in rndshift(np.where(notbound)[0]):
                    if optimize(i, j):
                        changed = True
                        break
                else:
                    # fallback-fallback second choice
                    log.debug('Fallback-fallback second choice activated')
                    for i in rndrange(m):
                        if optimize(i, j):
                            changed = True
                            break
        if examineAll:
            examineAll = False
            log.debug('Not examining all anymore')
        elif not changed:
            examineAll = True
            log.debug('Now examining all')

    return Svm(a, x, y, b, kern)
