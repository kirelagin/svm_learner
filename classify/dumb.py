import logging
log = logging.getLogger(__name__)
import numpy as np

def dumb(xs, ys):
    assert len(xs) == len(ys)

    def classify(x):
        f = np.where((xs == x).all(axis=1))[0]
        if f:
            return ys[f[0]]
        else:
            log.warn('Sample not found, returning 0')
            log.debug(x)
            return 0
    return classify
