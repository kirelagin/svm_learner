import logging
log = logging.getLogger(__name__)
from multiprocessing import Pool
import numpy as np


def classify_two(xs, ys, classes, i, j, learner, kern):
    log.info('Starting {} vs. {}'.format(i, j))
    # TODO: use classes here?
    ind = np.logical_or(ys == i, ys == j)
    xs = xs[ind]
    ys = ys[ind]
    r = learner(xs, np.vectorize(lambda y: 1 if y == i else 0)(ys) if len(ys) > 0 else np.array([]), kern)
    log.info('{} vs. {} done'.format(i, j))
    return r

class Ovo:
    def __init__(self, xs, ys, learner, kernFactory, kernParam=None):
        assert np.issubdtype(ys.dtype, int)
        assert len(xs) == len(ys)
        assert (ys >= 0).all()
        pool = Pool(3)

        self._n = n = ys.max() + 1
        log.debug('Found {} classes'.format(n))

        classes = [np.where(ys == c)[0] for c in range(n)]
        if kernParam is None:
            kern = kernFactory.guessParameters(xs, classes)
        else:
            kern = kernFactory(*kernParam)

        results = [pool.apply_async(classify_two, (xs, ys, classes, i, j, learner, kern)) for i in range(1, n) for j in range(i)]
        self._classifiers = [r.get() for r in results]

    def __call__(self, x):
        res = np.array([c(x) for c in self._classifiers])
        m = np.zeros((self._n, self._n))
        m[np.tril_indices(self._n, -1)] = res
        res = m.sum(axis=1) - m.sum(axis=0)
        log.debug(res)
        r = res.argmax()
        if res[r] < 0.5:
            log.warn('Probably bad prediction')
        log.debug(r)
        return r
