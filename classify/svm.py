import numpy as np


class Svm:
    def __init__(self, alphas, xs, ys, b, kern=np.dot):
        self._alphas = alphas
        self._xs = xs
        self._ys = ys
        self._b = b
        self._kern = kern

    def __call__(self, x):
        if len(self._xs) == 0: return self._b
        return np.prod([self._alphas, self._ys, np.apply_along_axis(lambda z: self._kern(z, x), 1, self._xs)], axis=0).sum() + self._b
