from classify.binary import Binary
from classify.multiclass import Ovo
from classify.svm import Svm
from classify.smo import ssmo, smo
