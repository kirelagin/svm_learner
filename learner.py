#!/usr/bin/env python3

from datetime import datetime
import logging
log = logging.getLogger(__name__)
import numpy as np
import os.path
import pickle
import mylog
from twitter import Twitter, OAuth

OAUTH_TOKEN = <put your token here> #!!!
OAUTH_SECRET = <put your secret here> #!!!
CONSUMER_KEY = 'kxU5MbR602ujTod2fwOZvQ'
CONSUMER_SECRET = '1DVRfckElqW2R2kwztLn0p73CeyjAYFox0HgQVpgsk'

from evaluate import evaluate
from classify import Ovo, smo
import read.stan as stan
import read.har as har
import read.wine as wine
import read.semeion as semeion
from util.kernel import Gaussian, Linear


def read_data(reader, setid):
    path = os.path.join('data', reader.__name__.split('.')[-1])
    return reader.read(path, setid)


if __name__ == '__main__':
    mylog.setup(logging.INFO)

    fh = logging.FileHandler('learner.log')
    formatter = logging.Formatter('%(asctime)s [%(name)s:%(levelname)s] %(message)s')
    fh.setFormatter(formatter)
    logging.getLogger().addHandler(fh)

    twit = Twitter(auth=OAuth(OAUTH_TOKEN, OAUTH_SECRET, CONSUMER_KEY, CONSUMER_SECRET))

    def learn(setkind, setid, kernFactory, testsetid):
        setkindname = setkind.__name__.split('.')[-1]
        log.info('STARTING {}'.format((setkindname, setid, kernFactory, testsetid)))
        log.info('Reading training data')
        trainX, trainY = read_data(setkind, setid)
        log.info('Training')
        svm = Ovo(trainX, trainY, smo, kernFactory)
        log.info('Ready')
        fname = '{}#{}_{}'.format(setkindname, setid, datetime.now().isoformat().replace(':', '-'))
        with open(os.path.join('runs', fname), 'wb') as f:
            pickle.dump(svm, f)

        log.info('Reading testing data')
        testX, testY = read_data(setkind, testsetid)
        log.info('Evaluating')
        percent = evaluate(svm, testX, testY)

        try:
            twit.statuses.update(status="@kirelagin Опа, я только что закончил обучаться на сете {}#{} / {}. Получил {:.2f}%!".format(setkindname, setid, kernFactory.__name__, percent))
        except:
            log.warn('Twit failed')


    tasks = [
             (har, ('train', '100good'), Gaussian, ('test', '100'))
            ]

    for t in tasks:
        learn(*t)

    twit.statuses.update(status="@kirelagin Ура, я всё сделал!!")
