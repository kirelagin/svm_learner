import itertools
import random


def rndshift(seq):
    n = len(seq)
    if n < 1:
        return seq
    r = random.randint(0, n-1)
    return itertools.islice(itertools.cycle(seq), r, r + n)

def rndrange(n):
    return rndshift(range(n))
