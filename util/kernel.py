import logging
log = logging.getLogger(__name__)
import numpy as np
from scipy.optimize import fmin


class Linear():
    def __init__(self, c = 0):
        self._c = np.float(c)

    def __call__(self, x1, x2):
        return np.dot(x1, x2) + self._c

    @staticmethod
    def guessParameters(xs, classes):
        # No-op
        return Linear(0)

class Gaussian:
    def __init__(self, sigma = 1):
        self._coef = np.float(1 / (2*sigma*sigma))

    def __call__(self, x1, x2):
        d = x1 - x2
        return np.exp(-np.dot(d, d) * self._coef)

    @staticmethod
    def guessParameters(xs, classes):
        log.info('Guessing parameters')
        n = len(classes)
        def w(sigma):
            g = Gaussian(sigma)
            return 1 / np.sum(len(c)**2 for c in classes) * np.sum([g(xs[l], xs[k]) for c in classes for l in c for k in c])
        def b(sigma):
            g = Gaussian(sigma)
            return 1 / np.sum(len(classes[i])*len(classes[j]) for i in range(n) for j in range(n) if i != j) * np.sum([g(xs[l], xs[k]) for i in range(n) for j in range(n) if i != j for l in classes[i] for k in classes[j]])

        def j(sigma):
            log.debug('eval at {}'.format(sigma))
            return 1 - w(sigma) + b(sigma)
        res = fmin(j, 6.0, xtol=0.01)
        sigma = res[0]

        log.info('sigma = {}'.format(sigma))
        return Gaussian(sigma)
