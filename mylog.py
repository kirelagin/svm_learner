import logging


def setup(level = logging.INFO, stream = None):
    level_map = {
        logging.DEBUG: (None, 'cyan', False),
        logging.INFO: (None, 'white', False),
        logging.WARNING: (None, 'yellow', True),
        logging.ERROR: (None, 'red', True),
        logging.CRITICAL: ('red', 'white', True),
    }

    nocolour = False
    try:
        from logutils.colorize import ColorizingStreamHandler

        ch = ColorizingStreamHandler()
        ch.level_map = level_map
        formatter = logging.Formatter('%(asctime)s [%(name)s] %(message)s')
    except ImportError:
        from logging import StreamHandler

        ch = StreamHandler()
        formatter = logging.Formatter('%(asctime)s [%(name)s:%(levelname)s] %(message)s')
        nocolour = True

    ch.setFormatter(formatter)
    root = logging.getLogger()
    root.addHandler(ch)
    root.setLevel(level)

    if nocolour:
        logging.getLogger('logging').warn('`logutils.colorize.ColorizingStreamHandler` could not be imported. Colour output disabled')
